# Language-usage statistics for websites

## Build
    
    docker-compose build
    docker push vvhung/flask_api
    cd vue_app && rm -rf ./hello/dist && docker-compose up --build && docker-compose down
    
## Deploy

    docker-compose up -d
    
There are few scripts for deploy in `deploy_folder`
    
## Usage

Access Vue app at `http://127.0.0.1:5400/`

There is a browser version Vue app at `http://127.0.0.1:5500/vue_browser/`