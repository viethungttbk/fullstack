#!/usr/bin/env bash

python3.6 -c "import nltk;
nltk.download('punkt')"

exec gunicorn run:app \
    --config /app/gunicorn_config.py \
    --log-level=info \
    --log-file=logs/server.log \
    --access-logfile=logs/server_access.log \
    --bind=0.0.0.0:${APP_PORT:-5500}





