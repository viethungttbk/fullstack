import os
import multiprocessing

num_workers = multiprocessing.cpu_count()

num_workers = min(num_workers, 1)
print('default workers')
print(num_workers)
workers = int(os.environ.get('WORKERS', num_workers))
locals()['workers'] = workers

worker_class = os.environ.get('CLASS', 'sync')
locals()['worker_class'] = worker_class

timeout = int(os.environ.get('TIMEOUT', 30))
locals()['timeout'] = timeout

keepalive = int(os.environ.get('KEEPALIVE', 3))  # increase if there is only one user (but sync user will ignore)
locals()['keepalive'] = keepalive

locals()['preload'] = True  # Load application code before the worker processes are forked
