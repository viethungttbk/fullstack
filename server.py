import os
import json
import shutil
import logging
import logging.handlers
from flask import request, jsonify, render_template
from flask_api import FlaskAPI, status
from flask_cors import CORS, cross_origin

# log_file = os.path.join('log', 'dev.log')
# logging.basicConfig(
#     level=logging.INFO,
#     format='[PID-%(process)d] %(asctime)s %(levelname)-7.7s %(message)s-[%(filename)s %(funcName)s line-%(lineno)d]',
#     handlers=[logging.handlers.TimedRotatingFileHandler(filename=log_file, interval=1, backupCount=5)]
# )

app = FlaskAPI(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'


@app.errorhandler(500)
def internal_error(error):
    return jsonify({'success': False, 'description': '', 'retVal': {'message': jsonify(error=error)}}), 500


@app.errorhandler(404)
def not_found(error):
    return jsonify({'success': False, 'description': 'Not found api!', 'retVal': ''}), 404


@app.route("/test/", methods=['GET', 'POST'])
def test():
    if request.method == 'POST':
        text = request.data.get('text')
    else:
        text = request.args.get('text')
    try:
        transcript = text
        return transcript
    except (IndexError, KeyError, TypeError):
        logging.exception('error')
        return 'error', status.HTTP_500_INTERNAL_SERVER_ERROR
    except FileNotFoundError:
        logging.exception('error')
        return 'file not found ', status.HTTP_400_BAD_REQUEST
    except ValueError as ve:
        logging.exception('error')
        return str(ve), status.HTTP_400_BAD_REQUEST


@app.route("/index/", methods=['GET', 'POST'])
def index():
    return render_template('index.html')


if __name__ == "__main__":
    app.run(debug=True, port=5500)
