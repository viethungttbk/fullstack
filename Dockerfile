FROM ubuntu:18.04 as python36

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends \
    software-properties-common apt-utils pkg-config vim git build-essential wget curl locales \
    python3.6 python3.6-dev python3-pip python3.6-venv python3.6-tk python3-setuptools

RUN locale-gen en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.en
ENV LC_ALL=en_US.UTF-8

RUN curl https://bootstrap.pypa.io/get-pip.py | python3.6 && \
    python3.6 -m pip install pip --upgrade

#-----------------------------------------------------------------------------------------------------------------------
FROM python36 as tool

RUN apt-get update && apt-get install -y --no-install-recommends \
     gcc g++ unzip mecab mecab-ipadic-utf8 libmecab-dev swig &&\
     rm -rf /var/lib/apt/lists/*

RUN pip3.6 install mecab-python3==0.7
RUN pip3.6 install jaconv==0.2.3

#-----------------------------------------------------------------------------------------------------------------------
FROM tool as requirements

COPY requirements.txt /requirements.txt
RUN pip3.6 install -r /requirements.txt

#-----------------------------------------------------------------------------------------------------------------------
FROM tool as main

COPY --from=requirements /usr/local /usr/local

# Set the working directory to /app
WORKDIR /app

CMD ["sh"]
