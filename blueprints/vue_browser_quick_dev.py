from flask import Blueprint, render_template, abort, request
from jinja2 import TemplateNotFound
import os
import json
import shutil
import logging
from flask_api import status
from services import most_frequent_words


vue_browser = Blueprint('vue_browser', __name__,
                    template_folder='templates',
                    static_folder='static')


@vue_browser.route('/', methods=['GET', 'POST'])
def index():
    try:
        return render_template('vue_app.html')
    except TemplateNotFound:
        abort(404)
