from flask import Blueprint, request
import os
import json
import shutil
import logging
from flask_api import status
from services import most_frequent_words


hist_api = Blueprint('hist_api', __name__)


@hist_api.route("/hist/", methods=['GET', 'POST'])
def api():
    if request.method == 'POST':
        urls = request.data.getlist('url[]')
    else:
        urls = request.args.getlist('url[]')
    try:
        result = json.dumps(most_frequent_words(urls), ensure_ascii=False)
        print(urls, result)
    except (IndexError, KeyError, TypeError):
        logging.exception('error')
        return 'error', status.HTTP_500_INTERNAL_SERVER_ERROR
    except FileNotFoundError:
        logging.exception('error')
        return 'file not found ', status.HTTP_400_BAD_REQUEST
    except ValueError as ve:
        logging.exception('error')
        return str(ve), status.HTTP_400_BAD_REQUEST
    return result


@hist_api.route("/hist_json/", methods=['GET', 'POST'])
def api_json():
    if request.method == 'POST':
        urls = request.data.get('url')
    else:
        urls = request.args.get('url')
    try:
        result = json.dumps(most_frequent_words(urls), ensure_ascii=False)
        print(urls, result)
    except (IndexError, KeyError, TypeError):
        logging.exception('error')
        return 'error', status.HTTP_500_INTERNAL_SERVER_ERROR
    except FileNotFoundError:
        logging.exception('error')
        return 'file not found ', status.HTTP_400_BAD_REQUEST
    except ValueError as ve:
        logging.exception('error')
        return str(ve), status.HTTP_400_BAD_REQUEST
    return result
