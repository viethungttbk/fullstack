from flask_socketio import SocketIO


def socket_app(app, host, post):
    socket_io = SocketIO()
    socket_io.init_app(app)
    socket_io.run(app, host=host, port=post)