from server import app
from .api import hist_api
from .vue_browser_quick_dev import vue_browser

app.register_blueprint(hist_api, url_prefix='/api/')
app.register_blueprint(vue_browser, url_prefix='/vue_browser')
