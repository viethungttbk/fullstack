import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './plugins/vue-i18n'
import './plugins/vuelidate'
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { version } from '../package.json'
// import vuetify from './plugins/vuetify';

/* eslint-disable no-console */
console.log(`App version: ${version}`)
console.log(`App locale: ${i18n.locale}`)
/* eslint-enable no-console */


Vue.config.productionTip = false

new Vue({
  router,
  store,
  i18n,
  // vuetify,
  render: h => h(App)
}).$mount('#app')
