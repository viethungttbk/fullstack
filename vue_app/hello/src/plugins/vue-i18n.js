import Vue from 'vue'
import VueI18n from 'vue-i18n'


Vue.use(VueI18n)

const messages = {
  vi: {
    message: {
      hello: 'Xin chào',
      instruction: 'Bạn muốn xem trang nào',
      submit: 'Gửi',
    }
  },
  en: {
    message: {
      hello: 'Hello',
      instruction: 'What url you want to get insight?',
      submit: 'Submit',
    }
  }
}


export default new VueI18n({
  fallbackLocale: 'en',
  locale: 'en',
  messages
})
