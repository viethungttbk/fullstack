from collections import Counter


def hist(word_list):
    return Counter(word_list)


def test():
    test_list = ['and', 'and', 'or', 'not', 'or']
    result = hist(test_list)
    assert result['and'] == 2 and result['or'] == 2 and result['not'] == 1
