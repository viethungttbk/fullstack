import os
from functools import reduce
from .crawling import get_one_page
from .crapping import get_clean_text
from .language import word_split
from .statistic import hist
from caching import memoize
from profile import howlong, exception
os.environ['CACHE'] = '1'


def compose(*tasks, decorator=lambda func: func):
    """
    Create a compose function from some functions after decorating each function with a decorator
    :param tasks: functions to compose
    :param decorator: a function that input a function and return a function
    :return: a composing function
    """
    # @memoize
    def pipeline(parameter):
        return reduce(lambda param, task: decorator(task)(param),
                      tasks,
                      parameter)
    return pipeline


helper = compose(
    exception(),  # add logging
    memoize,  # caching
    howlong,  # monitoring
)

one_page_hist = compose(
    get_one_page,
    get_clean_text,
    word_split,
    hist,
    decorator=helper
)


@memoize
def multi_pages_hist(urls: list):
    # functional way
    # return reduce(lambda x, y: x + y, map(one_page_hist, urls), hist([]))

    # recursive way
    if len(urls) < 1:
        return hist([])
    if len(urls) == 1:
        return one_page_hist(urls[-1])
    else:
        return multi_pages_hist(urls[:-1]) + multi_pages_hist(urls[-1:])


@memoize
def most_frequent_words(urls, n=100):
    unique_urls = list(set(urls))
    return multi_pages_hist(unique_urls).most_common(n)


def test():
    url = 'https://www.jpf.go.jp/j/index.html'
    # print(one_page_hist(url))
    print(multi_pages_hist([url, url]))