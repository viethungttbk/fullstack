# -*- coding: utf-8 -*-
from .word_segmentation import punkt
from .word_segmentation import japanese
from .word_segmentation import english
from .lang_detect import detect_language
from .language_split import get_languages_blocks
# from profile import howlong


def remove_values_from_list(the_list, vals):
    return [value for value in the_list if value not in vals]


def space_split(s):
    return remove_values_from_list(s.split(' '), [' ', ''])


def word_split_functions():
    return {
        **punkt.word_split_functions(),
        'english': english.word_split,
        'japanese': japanese.word_split,
        'latin': space_split,
        'character_split': lambda s: remove_values_from_list(list(s), [' ', '']),
    }


# @howlong
def _word_split(text, unicode_block):
    lang = detect_language(text)
    # print(text, lang)
    if lang not in word_split_functions():
        lang = unicode_block
    word_split_function = word_split_functions().get(lang, word_split_functions()['character_split'])
    return word_split_function(text)


# @howlong
def word_split(text):
    result = []
    languages_blocks = get_languages_blocks(text)
    for key, value in languages_blocks.items():
        for string in value:
            result.extend(_word_split(string, key))
    return result


def test():
    test_text = '私の名前は 中野です'
    assert ' | '.join(_word_split(test_text, 'unknown')) == '私 | の | 名前 | は | 中野 | です'
    assert ' | '.join(word_split(test_text)) == '私 | の | 名前 | は | 中野 | です'
    assert ' | '.join(word_split('I love,you')) == 'i | love | you'
