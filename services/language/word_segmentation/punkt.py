# -*- coding: utf-8 -*-
from nltk.tokenize import word_tokenize


def punkt_corpus():
    return [
        'czech',
        'danish',
        'dutch',
        'english',
        'estonian',
        'finnish',
        'french',
        'german',
        'greek',
        'italian',
        'norwegian',
        'portuguese',
        'slovene',
        'spanish',
        'swedish',
        'turkish',
    ]


def word_split_functions():
    return {
        lang: word_tokenize for lang in punkt_corpus()
    }