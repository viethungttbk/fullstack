from wordsegment import load, segment

load()


def word_split(text):
    return segment(text)


def test():
    assert ' | '.join(word_split("I love,you")) == 'i | love | you'


if __name__ == '__main__':
    test()