from langdetect import detect
from iso639 import languages


def support_language():
    return [
        'af',
        'ar',
        'bg',
        'bn',
        'ca',
        'cs',
        'cy',
        'da',
        'de',
        'el',
        'en',
        'es',
        'et',
        'fa',
        'fi',
        'fr',
        'gu',
        'he',
        'hi',
        'hr',
        'hu',
        'id',
        'it',
        'ja',
        'kn',
        'ko',
        'lt',
        'lv',
        'mk',
        'ml',
        'mr',
        'ne',
        'nl',
        'no',
        'pa',
        'pl',
        'pt',
        'ro',
        'ru',
        'sk',
        'sl',
        'so',
        'sq',
        'sv',
        'sw',
        'ta',
        'te',
        'th',
        'tl',
        'tr',
        'uk',
        'ur',
        'vi',
        'zh-cn',
        'zh-tw',
    ]


def detect_language(text):
    try:
        language = languages.get(alpha2=detect(text).split('-')[0])
        return language.name.lower()
    except:
        return 'unknown'


def is_ja(text):
    return detect(text) == 'ja'


def test():
    assert detect_language('天聪二年，太宗伐察哈尔多罗特部，破敌於敖穆楞，多尔衮有功，赐号墨尔根代青。') == 'chinese'
    assert is_ja('私の名前は 中野です')


if __name__ == '__main__':
    test()