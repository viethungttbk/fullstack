from textblob import TextBlob
from iso639 import languages
from profile import exception


@exception(exception_return='unknown')
def detect_language(text):
    if len(text) < 3:
        return 'unknown'
    language = languages.get(alpha2=TextBlob(text).detect_language().split('-')[0])
    return language.name.lower()


def is_ja(text):
    return TextBlob(text).detect_language() == 'ja'


def test():
    assert detect_language('天聪二年，太宗伐察哈尔多罗特部，破敌於敖穆楞，多尔衮有功，赐号墨尔根代青。') == 'chinese'
    assert is_ja('私の名前は 中野です')


if __name__ == '__main__':
    test()