# -*- coding: utf-8 -*-
import os


def get_unicode_blocks(data_file='unicode_blocks.txt', start_line=35, end_line=334):
    """
    Read Unicode Character Database (ftp://www.unicode.org/Public/UNIDATA/Blocks.txt) to get unicode blocks.
    :param data_file: text file contain data in lines
    :param start_line: attention data start line (count from 1)
    :param end_line: attention data end line (count from 1)
    :return: dictionary with key = block_name, value = block range [start, end]
    """
    result = {}
    data_file_path = os.path.join(os.path.dirname(__file__), data_file)
    if os.path.isfile(data_file_path):
        with open(data_file_path, 'r', encoding='utf-8') as f:
            data = f.readlines()
            info_lines = data[start_line-1:end_line]
            for line in info_lines:
                parts = line.split('; ')
                block_name = parts[-1].strip()
                start, end = parts[0].split('..')
                result[block_name] = (start, end)
        return result
    else:
        import re
        import ftplib

        def parse(line):
            pattern = '^(?P<from>[0-9A-F]+)\.\.(?P<to>[0-9A-F]+); (?P<name>.+)$'
            _parts = re.findall(pattern, line)
            if len(_parts) > 0:
                print(_parts)
                _block_name = _parts[0][-1]
                _start, _end = _parts[0][0:2]
                result[_block_name] = (_start, _end)

        ftp = ftplib.FTP('www.unicode.org')
        ftp.login()
        ftp.retrlines('RETR Public/UNIDATA/Blocks.txt', parse)
        return result


unicode_blocks = get_unicode_blocks()


def test():
    assert unicode_blocks['Basic Latin'] == ('0000', '007F')
    assert unicode_blocks['Khmer'] == ('1780', '17FF')
    assert unicode_blocks['Supplementary Private Use Area-B'] == ('100000', '10FFFF')
    
    
if __name__ == '__main__':
    print(unicode_blocks)