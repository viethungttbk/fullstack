def is_space_split(text):
    if len(text) < 20:
        raise ValueError('Input is too short!')
    number_of_space = text.count(' ')
    if number_of_space/len(text) < 0.05:
        return False
    else:
        return True


def test():
    assert is_space_split('Please think carefully')
    assert not is_space_split('田中さんにあげて下さい。 田中さんにあげて下さい。')