# -*- coding: utf-8 -*-
import re
from .unicode_blocks import unicode_blocks


latin_blocks_pattern = '[{}]+'.format(''.join([
    '{}-{}'.format(*[chr(int(char, 16)) for char in value])
    for key, value in unicode_blocks.items()
    if 'Latin' in key
]))


japanese_blocks_patter = '[{}]+'.format(''.join([
    '{}-{}'.format(*[chr(int(char, 16)) for char in value])
    for key, value in unicode_blocks.items()
    if 'CJK' in key or key in ['Hiragana', 'Katakana', 'Kana Supplement', 'Kana Extended-A', 'Small Kana Extension']
]))


support_blocks_patter = {
    'latin': latin_blocks_pattern,
    'japanese': japanese_blocks_patter,
}


def get_languages_blocks(text):
    result = {
        'latin': [],
        'japanese': [],
    }
    tmp = text
    for key, pattern in support_blocks_patter.items():
        language_blocks = re.findall(pattern, tmp)
        tmp = re.sub(pattern, '', tmp)
        if len(language_blocks) > 0:
            result[key].extend(language_blocks)
    if len(tmp) > 0:
        result['Other'] = [tmp]
    return result


def test():
    text = '''
    田中さんにあげて下さい。 Tanaka-san ni agete kudasai "Please give it to Mr. Tanaka."
    '''
    languages_blocks = get_languages_blocks(text)
    assert languages_blocks['Basic Latin'][0] == '\n    '
    assert languages_blocks['CJK'][0] == '。'
    assert languages_blocks['Hiragana'][0] == 'さんにあげて'
    
    
if __name__ == '__main__':
    test_text = '''
    Chung cư Hausneo, Ngã tư Liên Phường, Võ Chí Công, Phường Phú Hữu, Q9, Tp Hồ Chí Minh. 
    田中さんにあげて下さい。 Tanaka-san ni agete kudasai "Please give it to Mr. Tanaka.
    '''
    print(get_languages_blocks(test_text))