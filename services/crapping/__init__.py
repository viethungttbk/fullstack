from .special_character import remove_special_character
from .html_visulized_text import visualized_text_from_html


def get_clean_text(html):
    return remove_special_character(visualized_text_from_html(html))
