from bs4 import BeautifulSoup
from bs4.element import Comment


def visualized_html_tag(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        return False
    if isinstance(element, Comment):
        return False
    return True


def visualized_text_from_html(body):
    soup = BeautifulSoup(body, 'html.parser')
    texts = soup.findAll(text=True)
    visible_texts = filter(visualized_html_tag, texts)
    # for t in visible_texts:
    #     print(t.parent, t.strip())
    return ' '.join(t.strip() for t in visible_texts).strip()


def test():
    html = '''<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script>
        var x = 1;
    </script>
    <style>
        div {
            border: none;
        }
    </style>
</head>
<body>
    <p>
        test
    </p>

</body>
</html>'''
    assert visualized_text_from_html(html) == 'test'


if __name__ == '__main__':
    import urllib.request
    html = urllib.request.urlopen('https://www.jpf.go.jp/j/index.html').read()
    visible_text_string = visualized_text_from_html(html)
    print(visible_text_string)
