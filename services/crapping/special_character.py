import re


def remove_special_character(text):
    common_symbols = '!@#$%\^&*()_\-+={[}\]|\\\\;:"<>?/'
    language_symbols = '・、。'
    symbols = '[{}{}]+'.format(common_symbols, language_symbols)
    return re.sub(symbols, '', text)


def test():
    test_text = 'abc@xyz is @linh \email'
    assert remove_special_character(test_text) == 'abcxyz is linh email'
