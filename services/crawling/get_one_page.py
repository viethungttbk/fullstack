import urllib.request
import urllib.error


def get_one_page(url):
    try:
        return urllib.request.urlopen(url, timeout=10).read()
    except urllib.error.URLError as ex:
        if 'timed out' in str(ex):
            raise ValueError('Timeout (> 10s)')
        raise ValueError('Unknown address')


if __name__ == '__main__':
    url = 'http://35.167.101.10:5500/demo/'
    print(get_one_page(url))