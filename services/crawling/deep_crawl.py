# -*- coding: utf-8 -*-
from urllib.parse import urljoin
from bs4 import BeautifulSoup
from .get_one_page import get_one_page


def is_child(parent_url, child_full_path, level=1):
    parent_base_url = parent_url
    if any([parent_url.endswith(ext) for ext in ('.html', '.htm', '.php', '.asp')]):
        parent_base_url = parent_url.rsplit('.', 1)[0]
    if child_full_path[:len(parent_base_url)] == parent_base_url:
        remaining = child_full_path[len(parent_base_url):].split('/')
        remaining.remove('')
        print(parent_base_url, remaining)
        if 0 < len(remaining) <= level:
            return True
    return False


def get_child_links(url, deep=1):
    result = [url]
    print(url)
    html = get_one_page(url)

    soup = BeautifulSoup(html, "lxml")
    base = soup.find("base")
    base_url = url
    if base and base.attrs.get('href'):
        base_url = base.attrs['href']
    for link in soup.find_all('a'):
        try:
            href = link.attrs.get('href')
            if href:
                child_full_path = urljoin(base_url, href)
                if is_child(url, child_full_path, level=deep):
                    result.append(child_full_path)
        except ValueError:
            continue
        except IndexError:
            continue

    return result


def test():
    assert is_child('http://tuoitre.vn/tin/the-gioi', 'http://tuoitre.vn/tin/the-gioi/today/9h', level=2)
    print(get_child_links('https://tuoitre.vn/giao-duc.htm', deep=3))