#!/usr/bin/env bash

docker pull vvhung/flask_api
git clone https://gitlab.com/viethungttbk/fullstack.git

# build vue_app
cd fullstack/vue_app && rm -rf ./hello/dist && docker-compose up --build && docker-compose down

cd ..
docker-compose up -d