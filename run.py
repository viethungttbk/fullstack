import os
import json
import shutil
import logging
from flask import request, render_template
from flask_api import status
from blueprints import app
from services.crawling import get_child_links


@app.route("/api/demo/", methods=['GET', 'POST'])
def api():
    if request.method == 'POST':
        url = request.data.get('url')
        deep = request.data.get('deep', 2)
    else:
        url = request.args.get('url')
        deep = request.args.get('deep', 2)
    try:
        result = json.dumps(get_child_links(url, int(deep)), ensure_ascii=False)
        print(url, result)
    except (IndexError, KeyError, TypeError, AttributeError):
        logging.exception('error')
        return 'error', status.HTTP_500_INTERNAL_SERVER_ERROR
    except FileNotFoundError:
        logging.exception('error')
        return 'file not found ', status.HTTP_400_BAD_REQUEST
    except ValueError as ve:
        logging.exception('error')
        return str(ve), status.HTTP_400_BAD_REQUEST
    return result


@app.route("/api/demo_json/", methods=['GET', 'POST'])
def api_json():
    if request.method == 'POST':
        url = request.data.get('url')
        deep = request.data.get('deep', 2)
    else:
        url = request.args.get('url')
        deep = request.args.get('deep', 2)
    try:
        result = json.dumps(get_child_links(url, int(deep)), ensure_ascii=False)
        print(url, result)
    except (IndexError, KeyError, TypeError, AttributeError):
        logging.exception('error')
        return 'error', status.HTTP_500_INTERNAL_SERVER_ERROR
    except FileNotFoundError:
        logging.exception('error')
        return 'file not found ', status.HTTP_400_BAD_REQUEST
    except ValueError as ve:
        logging.exception('error')
        return str(ve), status.HTTP_400_BAD_REQUEST
    return result


@app.route("/demo/", methods=['GET'])
def demo():
    return render_template('form_example.html')


if __name__ == "__main__":
    app.run(debug=True, port=5500, host='0.0.0.0')
