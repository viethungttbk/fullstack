user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
  worker_connections  1024;  ## Default: 1024, increase if you have lots of clients
}

http {
    include       /etc/nginx/mime.types;
    # fallback in case we can't determine a type
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    '$status $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;
    keepalive_timeout  65;

    upstream app {
        server flask_api:5500;
    }
    server {
        listen 80;
        # server_name "";
        charset utf-8;

        satisfy any;

        # Begin of IP whitelist
        # allow 183.91.7.250;
        # End of IP whitelist
        # deny all;

        # auth_basic "Staff's Area";
        # auth_basic_user_file /etc/nginx/htpasswd;  # root/rossa

        location / {
            try_files $uri $uri/ /index.html;
            root   /usr/share/nginx/html;
            index  index.html index.htm;
            include /etc/nginx/mime.types;
        }

        # Handle noisy favicon.ico messages in nginx
        # location = /favicon.ico {
        #     return 204;
        #     access_log     off;
        #     log_not_found  off;
        # }

        location /api/ {
            # checks for static file, if not found proxy to app
            try_files $uri @proxy_to_app;
            client_max_body_size 100M;
        }

        location @proxy_to_app {
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;
            proxy_pass http://app;
            expires -1;
            client_max_body_size 100M;
            proxy_read_timeout 1d;
            proxy_send_timeout 1d;
        }
    }
}