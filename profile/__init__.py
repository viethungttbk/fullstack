import logging
from time import time
from functools import wraps


def howlong(func):
    """
    Decorator to print a function's execution time
    Time taken for the most recent call to the decorated function can be accessed via the `last_run` attribute
    :param func: function to measure execute time
    :return: last executed time of function
    """

    @wraps(func)
    def timed_func(*args, **kwargs):
        start_time = time()
        result = func(*args, **kwargs)
        stop_time = time()
        timed_func.last_run = stop_time - start_time
        print(f'Calling {func.__name__} took {timed_func.last_run}')
        return result

    return timed_func


def exception(exception_return=None):
    def wrapper(func):
        @wraps(func)
        def log_func(*args, **kwargs):
            try:
                result = func(*args, **kwargs)
                return result
            except Exception as ex:
                logging.exception('{}(args={}, kwargs={})'.format(func.__name__, args, kwargs))
                if exception_return:
                    return exception_return
                else:
                    raise ex
        return log_func
    return wrapper
