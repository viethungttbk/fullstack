import os
import sys
import json
import shutil
import pickle
import xxhash
from glob import glob
from time import time
from functools import wraps


def memoize(func):
    '''Cache result of function call on disk
    Support multiple positional and keyword arguments'''

    def print_status(status, func, args, kwargs):
        print(f'{status}\n'
              f'func   :: {func.__name__}\n'
              # f'args   :: \n'
              # f'{args}\n'
              # f'kwargs :: \n'
              # f'{kwargs}'
        )
        pass

    def identify(x):
        '''Return an hex digest of the input'''
        return xxhash.xxh64(pickle.dumps(x), seed=0).hexdigest()

    @wraps(func)
    def memoized_func(*args, **kwargs):
        cache_dir = 'cache'
        try:
            os.environ['CACHE']
            print(
                'Environment variable CACHE is set, will use cache when possible\n'
                'To invalidate cache, add the function name as an environment variable'
            )
            func_id = identify((func.__name__, args, kwargs))
            cache_path = os.path.join(cache_dir, func.__name__, func_id)
            if not os.path.isdir(os.path.dirname(cache_path)):
                os.makedirs(os.path.dirname(cache_path))
            if (os.path.exists(cache_path) and
                    not func.__name__ in os.environ and
                    not 'BUST_CACHE' in os.environ):
                print_status('Using cached result', func, args, kwargs)
                return pickle.load(open(cache_path, 'rb'))
            else:
                print_status('Updating cache with fresh run', func, args,
                             kwargs)
                result = func(*args, **kwargs)
                if not os.path.exists(cache_dir):
                    os.mkdir(cache_dir)
                pickle.dump(result, open(cache_path, 'wb'))
                return result
        except (KeyError, AttributeError, TypeError):
            print_status('Not using or updating cache ', func, args, kwargs)
            return func(*args, **kwargs)

    return memoized_func
